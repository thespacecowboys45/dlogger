DLOGGER
=======
ENHANCED DEBUG LOGGER FOR GOLANG. Accepts debug levels, and debug event types for logging.

INSTALLATION
------------
Clone into the 'src' folder of your GOPATH


INFORMATION
-----------
Levels: { always | debug | info | error }
Order of precendence:
  - always <- always print out log
  - debug <- prints 'debug' 'info' and 'error' logs
  - info <- prints out 'info' and 'error levels
  - error <- prints out 'error' level logs

EVENTS
----------
You can define your own event types.  For example, if your program receives HTTP requests, you can
create an event type called "http".  Then, whenever an HTTP request comes in you can log against
this event type.

If the event type "http" is enabled (in the example above), then a log statement will get output.

The idea here is that we often have only certain sections of code that we are interested in
debugging.  By creating an event type, and logging against that event type, we can see only
the information that is relevant to the debugging session.



USAGE
-----
```
package main

import(
     
     "fmt"
     "dlogger"
)

func main() {
    dlogger.Always("[events.go][Test()][entry]")
    dlogger.Debug("[events.go][Test()][entry]")
    dlogger.Info("[events.go][Test()][entry]")
    dlogger.Error("[events.go][Test()][entry]")
    dlogger.Always(fmt.Sprintf("How to log a variable: %s", msg))
    dlogger.SetLevel('always')
    dlogger.SetLevel('debug')
    dlogger.SetLevel('info')
    dlogger.SetLevel('error')
    level := dlogger.GetLevel()
    
    dlogger.EnableEvent('trace')
    dlogger.DisableEvent('trace')
    events := dlogger.GetEventsEnabled()
}
```

CHANGELOG
---------
##### v7.0
- add a list of to store all logging events available
- add web GUI for controlling events enabled

##### v6.0
* add web handler

##### v5.0
* add events

##### v4.0
* Fix issue nesting fmt.Printf(fmt.Sprintf()) if the output string has a percent-sign in it

##### v3.0
* implement variadic function to take a variable number of parameters to print out

##### v2.0
* initial revision

##### v1.0
* skipped


TODO
-----
- activate all logging levels
- add web menu for events