package dlogger
/**
 * Based loosely on: https://godoc.org/github.com/alexcesaro/log/stdlog
 *
 * Concepts
 *   Levels - info, warning, etc.
 *   Events - http, metric, network related type events etc.
 *   Event Groups - enables a certain set number of events based on an "group" setting
 */

import(
	"log"
	"os"
	"fmt"
	"errors"
	"net/http"
	"strings"
)

/********************************************************************
 * GLOBALS GLOBALS GLOBALS GLOBALS GLOBALS GLOBALS GLOBALS GLOBALS  *
 ********************************************************************/
var DloggerVersion = "6.0" // the revision of this logger code
var TheCurrentDLogLevel string
var EventsEnabled []string
var EventsAvailable []string

func GetLevel() string {
	return TheCurrentDLogLevel
}

func SetLevel(level string) {
	log.Printf("dlogger.SetLevel to %s", level)
	TheCurrentDLogLevel = level
}

func dLog(msg string, level string) {
	
	// always print these messages
	if level == "always" || level == "event" {
		log.Printf("[%s] %s", level, msg)
		return
	}
	
	// What is the debug logging level we're currently set at, and
	// does it match with the log_level of the message we want to print
	if TheCurrentDLogLevel == "debug" {
		if level == "debug" || level == "info" || level == "error" {
			log.Printf("[%s] %s", level, msg)
		}
	} else if TheCurrentDLogLevel == "info" {
		if level == "info" || level == "error" {
			log.Printf("[%s] %s", level, msg)
		}
	} else if TheCurrentDLogLevel == "error" {
		if level == "error" {
			log.Printf("[%s] %s", level, msg)
		}
	}
}

func Always(format string, a ...interface{}) {
    // cannot nest fmt.Sprintf if the output has a percent sign '%'
    output := fmt.Sprintf(format, a...)
    dLog(output, "always")
}

func Debug(format string, a ...interface{}) {
    // cannot nest fmt.Sprintf if the output has a percent sign '%'
    output := fmt.Sprintf(format, a...)
    dLog(output, "debug")
}

func Info(format string, a ...interface{}) {
    // cannot nest fmt.Sprintf if the output has a percent sign '%'
    output := fmt.Sprintf(format, a...)
    dLog(output, "info")
}

func Notice(format string, a ...interface{}) {
    // cannot nest fmt.Sprintf if the output has a percent sign '%'
    output := fmt.Sprintf(format, a...)
    dLog(output, "notice")
}

func Warning(format string, a ...interface{}) {
    // cannot nest fmt.Sprintf if the output has a percent sign '%'
    output := fmt.Sprintf(format, a...)
    dLog(output, "warning")
}

func Error(format string, a ...interface{}) {
    // cannot nest fmt.Sprintf if the output has a percent sign '%'
    output := fmt.Sprintf(format, a...)
    dLog(output, "error")
}

func Critical(format string, a ...interface{}) {
    // cannot nest fmt.Sprintf if the output has a percent sign '%'
    output := fmt.Sprintf(format, a...)
    dLog(output, "critical")
}

func Alert(format string, a ...interface{}) {
    // cannot nest fmt.Sprintf if the output has a percent sign '%'
    output := fmt.Sprintf(format, a...)
    dLog(output, "alert")
}

func Emergency(format string, a ...interface{}) {
    // cannot nest fmt.Sprintf if the output has a percent sign '%'
    output := fmt.Sprintf(format, a...)
    dLog(output, "emergency")
}

func stringInSlice(str string, list []string) bool {
 	for _, v := range list {
 		if v == str {
 			return true
 		}
 	}
 	return false
 }

// Log a specific event that has occured IF enabled
func LogEvent(msg string, eventType string) {
	if stringInSlice(eventType, EventsEnabled) {
		dLog(fmt.Sprintf("[ %s ] %s", eventType, msg), "event")
	}
}

// HTML Specific functions
func dloggerWriteHtmlHeader(w http.ResponseWriter) {
    header := `<!DOCTYPE html>
                     <html>
                     <head>
                         <meta charset="UTF-8" />
                     </head>
                     <body>`
    fmt.Fprintf(w, string(header))
}

func dloggerWriteHtml(w http.ResponseWriter) {
    fmt.Fprintf(w, fmt.Sprintf("<table><tr><td>Current debugging log level</td><td>'%s'</td></tr>\n", TheCurrentDLogLevel))
}

func dloggerWriteEventSelect(w http.ResponseWriter) {

    fmt.Fprintf(w, `<table><tr>`)

    for _, e := range EventsAvailable {
        fmt.Fprintf(w, fmt.Sprintf("<tr><td>%s</td>", e))
        fmt.Fprintf(w, fmt.Sprintf("<td><input type=\"checkbox\" name=\"event_%s\" ", e))
        if IsEventEnabled(e) {
            fmt.Fprintf(w, "CHECKED")
        }
        fmt.Fprintf(w, fmt.Sprintf("></td></tr>"))
    }

    fmt.Fprintf(w, `</tr></table>`)
}


func dloggerWriteLogLeveLSelect(w http.ResponseWriter) {
    table := `<table>
                        <tr><td>Select desired level</td>
                            <td>
                              <select name="log_level">
                                <option value="debug">Debug</option>
                                <option value="info">Info</option>
                                <option value="notice">Notice</option>
                                <option value="warning">Warning</option>
                                <option value="error">Error</option>
                                <option value="critical">Critical</option>
                                <option value="warning">Alert</option>
                                <option value="emergency">Emergency</option>
                              </select>
                            </td>
                            <td><input type="submit" value="submit" /></td>
                        </tr>
                      </table>`
    fmt.Fprintf(w, table)
}

func dloggerWriteHtmlForm(w http.ResponseWriter) {

    fmt.Fprintf(w, `<form method="POST" action="/dlogger">`)
    dloggerWriteLogLeveLSelect(w)
    dloggerWriteEventSelect(w)
    fmt.Fprintf(w, `</form>`)

}

func dloggerWriteHtmlFooter(w http.ResponseWriter) {
    footer := `</body></html>`
    fmt.Fprintf(w, string(footer))
}

func dloggerIndexHtml(w http.ResponseWriter, r *http.Request) {
    dloggerWriteHtmlHeader(w)
    dloggerWriteHtml(w)
    dloggerWriteHtmlForm(w)
    dloggerWriteHtmlFooter(w)
}

func dloggerHandlePost(w http.ResponseWriter, r *http.Request) {
    if err := r.ParseForm(); err != nil {
        Error(fmt.Sprintf("Dlogger cannot parse form: %s", err))
        return
    }

    // Reset the array
    EventsEnabled = EventsEnabled[:0]

    for k, v := range r.PostForm {
        Always("Form post %s=%s", k, v)
        // If the name of the form element begins with 'event_' interpret it as an eventType to enable
        if strings.HasPrefix(k, "event_") {
            // Get the rest of the string value and use it to enable the events
            eventType := k[6:]
            Always("Enable event: %s", k[6:])
            EnableEvent(eventType)
        }
    }

    log_level := r.FormValue("log_level")
    fmt.Fprintf(w, fmt.Sprintf("<div>Set new log level to: '%s'</div>", log_level))
    SetLevel(log_level)
}

func dloggerHandler(w http.ResponseWriter, r *http.Request) {
    if r.URL.Path != "/dlogger" {
        http.Error(w, "404 not found.", http.StatusNotFound)
    }

    switch r.Method {
    case "GET":
        dloggerIndexHtml(w, r)
    case "POST":
        dloggerWriteHtmlHeader(w)
        dloggerHandlePost(w, r)
        dloggerWriteHtmlForm(w)
        dloggerWriteHtml(w)
    default:
        fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")
    }
}

/**
 * init()
 *
 * Send all output to stdout
 * Set default log level to 'info'
 */
func init() {
	log.Printf("Dlogger init() version: %s", DloggerVersion)
	log.SetOutput(os.Stdout)
	TheCurrentDLogLevel = "info"

	http.HandleFunc("/dlogger", dloggerHandler)
}

/*
 * DAVB - I'll figure this out later, how to set and use global variables in a struct
  
type Dlog struct {
	level string
}
func (d *Dlog) GetLevel() string {
	log.Printf("GetLevel current level is %s", d.level)
	return TheDLogLevel
}

func (d *Dlog) SetLevel(level string) {
	log.Printf("SetLevel to %s", level)
	d.level = level
}
*/

func RegisterEvent(eventType string) error {
    dLog(fmt.Sprintf("RegisterEvent '%s'", eventType), "always")
    if stringInSlice(eventType, EventsAvailable) {
        return errors.New(fmt.Sprintf("Event '%s' already registered", eventType))
    }
    EventsAvailable = append(EventsAvailable, eventType)
    return nil
}

func EnableEvent(eventType string) error {
	dLog(fmt.Sprintf("EnableEvent '%s'", eventType), "always")
	if stringInSlice(eventType, EventsEnabled) {
	    return errors.New(fmt.Sprintf("Event '%s' already enabled", eventType))
    }
    EventsEnabled = append(EventsEnabled, eventType)
    return nil
}

// Find out if an event is enabled
func IsEventEnabled(eventType string) bool {
    if stringInSlice(eventType, EventsEnabled) {
        return true
    }
    return false
}

func DisableEvent(eventType string) error {
    
	Always("DisableEvent '%s'", eventType)
	if ! stringInSlice(eventType, EventsEnabled) {
	    return errors.New(fmt.Sprintf("Event '%s' is not enabled", eventType))
	}
	
	// Look at all the elements in the slice
	for k, v := range EventsEnabled {
	    // Found it
 		if v == eventType {
 		    // Delete the item from the slice
 			EventsEnabled = append(EventsEnabled[:k], EventsEnabled[k+1:]...)
 			break
 		}
 	
	}
	
	return nil
}

func GetEventsEnabled() []string {
    return EventsEnabled
}